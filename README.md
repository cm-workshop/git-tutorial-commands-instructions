# GIT-TUTORIAL-COMMANDS-INSTRUCTIONS (git basic cli)

## Tools

Using

[git visualize](https://git-school.github.io/visualizing-git/)

for visualizing the branches and tracking versions of the repository (for learning)

## Description

Git commands are used for sharing and combining the code easily with other developers.

## Git Commands

Following are the some basic Git commands can be used to work with Git −

## Installation

[Visit git-scm.sh for more information](https://git-scm.com/downloads)

## The version of the Git can be checked by using the below command −

```bash
$ git --version
```

## Git Authentication

### Add Git username and email address to identify the author while committing the information. Set the username by using the command as −

```bash
$ git config --global user.name "USERNAME"
```

### After entering user name, verify the entered user name with the below command −

```bash
$ git config --global user.name
```

### Next, set the email address with the below command −

```bash
$ git config --global user.email "email_address@example.com"
```

### You can verify the entered email address as −

```bash
$ git config --global user.email
```

### Use the below command to check the entered information −

```bash
$ git config --global --list
```

[Readme about basic commands](/readme/readme_basic_commands.md)
