# SSH Keys configuration - For easy to do the following command

1. Locate `.ssh` folder in `~/.ssh`

2. Generate key pairs

```bash
ssh-keygen -t rsa -C example@mail.com
```

3. Drop `id_rsa.pub` from local to `~/.ssh/authorized_keys` of server

4. Testing connection

```bash
ssh $USER@$IP_ADDRESS
```
