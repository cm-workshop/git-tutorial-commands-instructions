# Create a new project

## Way to create a new project have 2 way

### 1. Create by "git init"

Create a new directory for your project:

```bash
mkdir project_name
```

Change into the project directory:

```bash
cd project_name
```

Initialize a new Git repository:

```bash
git init
```

Now, your project is set up with Git version control.

### 2. Create on version control application

1. Create a new project on gitlab

[Create new](https://gitlab.com/projects/new)

2. clone/remote repository to local

- clone (cloning repository from gitlab to local)

```bash
git clone <https-repository.git>
```

- remote (if you already have existing repositories)

```bash
cd existing_repo # that have .git folder
git remote add origin <https-repository.git>
git branch -M main
git push -uf origin main
```

[Visit basic workflow](/readme/readme_git_basic_flow.md)
