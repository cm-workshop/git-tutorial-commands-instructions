# Branching

Branching allows you to work on separate features or fixes without affecting the main codebase. Here's how to use branches:

Create a new branch:

```bash
 git branch branch_name
```

Switch to the new branch:

```bash
git checkout branch_name
# OR
git switch branch_name # (Git version 2.23+)
```

Make changes and commit them on the new branch.
Switch back to the main branch:

```bash
git checkout main
#OR
git switch main
```

Merge the changes from the branch into the main branch:

```bash
git merge branch_name
```
