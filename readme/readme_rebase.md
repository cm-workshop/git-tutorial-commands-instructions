# Rebase operations

Rebasing is a way to integrate changes from one branch to another, making the commit history cleaner.
It moves the base of your branch to the latest commit in the main branch.

Make sure you're on your feature branch:

```bash
git checkout feature_branch
```

Rebase the branch onto the main branch:

```bash
git rebase main
```

Deal with any conflicts (if any).
After resolving conflicts, continue the rebase:

```bash
git rebase --continue
```

If you want to abort the rebase:

```bash
git rebase --abort
```
