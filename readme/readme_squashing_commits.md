# Squashing commits

Squashing allows you to combine multiple commits into one, making your commit history more concise.

1. Identify the commit hashes you want to squash using `git log`.
2. Interactively rebase the commits (replace "n" with the number of commits to squash):

```bash
git rebase -i HEAD~n
```

3. In the editor, replace "pick" with "squash" for the commits you want to squash.

4. Save and close the editor.

5. Edit the commit message that combines all the squashed commits.
