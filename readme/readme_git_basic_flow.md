# Git Basic FLow

1. changing something to repository

Adding text.txt file

```bash
echo "HELLO-WORLD" < "text.txt"
```

2. Trace the changes by `git add`

```bash
git add . # trace all changes
```

3. Commit the changes

```bash
git commit -m "Hello, world!"
```

4. Push to repository

```bash
git push origin main
```
