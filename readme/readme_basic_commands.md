# git commands

## Branching

### You can pull the latest changes made to the master branch by using the below command −

```bash
$ git checkout master
```

### You can fetch the latest changes to the working directory with the below command −

```bash
$ git pull origin NAME-OF-BRANCH -u
```

### Here, NAME-OF-BRANCH could be 'master' or any other existing branch.

Create a new branch with the below command −

```bash
$ git checkout -b branch-name
```

You can switch from one branch to other branch by using the command as −

```bash
$ git checkout branch-name
```

## Status/add/push

### Check the changes made to your files with the below command −

```bash
$ git status
```

### You will see the changes in red color and add the files to staging as −

```bash
$ git add file-name
```

Or you can add all the files to staging as −

```bash
$ git add *
```

### Now send your changes to master branch with the below command −

```bash
$ git push origin branch-name
```

### Delete the all changes, except unstaged things by using the below command −

```bash
$ git checkout .
```

### You can delete the all changes along with untracked files by using the command as −

```bash
$ git clean -f
```

## Merge

### To merge the different branch with the master branch, use the below command −

```bash
$ git checkout branch-name
$ git merge master
```

### You can also merge the master branch with the created branch, by using the below command −

```bash
$ git checkout master
$ git merge branch-name
```
